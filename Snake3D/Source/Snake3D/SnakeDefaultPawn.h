// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "SnakeDefaultPawn.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UStaticMeshComponent;
class USceneComponent;

/**
 * 
 */
UCLASS()
class SNAKE3D_API ASnakeDefaultPawn : public ADefaultPawn
{
	GENERATED_BODY()
	
public:

	ASnakeDefaultPawn();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Category = "Movement")
		virtual	void InputMoveForward(float AxisValue);

	UFUNCTION(Category = "Movement")
		virtual	void InputMoveRight(float AxisValue);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
		USpringArmComponent* SpringArm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Components")
		UCameraComponent* CameraComponent;
};
