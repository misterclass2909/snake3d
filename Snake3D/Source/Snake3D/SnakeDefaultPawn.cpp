// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeDefaultPawn.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"

ASnakeDefaultPawn::ASnakeDefaultPawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Turn off default movement bindings
	this->bAddDefaultMovementBindings = false;

	//Create camera components
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring arm"));
	SpringArm->TargetArmLength = 450.f;
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	//Attach camera to spring arm
	CameraComponent->SetupAttachment(SpringArm);

	//Attach to root
	SpringArm->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASnakeDefaultPawn::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakeDefaultPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakeDefaultPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASnakeDefaultPawn::InputMoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASnakeDefaultPawn::InputMoveRight);
}

void ASnakeDefaultPawn::InputMoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector(), AxisValue);
	UE_LOG(LogTemp, Warning, TEXT("%d"), AxisValue);
}

void ASnakeDefaultPawn::InputMoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector(), AxisValue);
}
